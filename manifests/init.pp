#fips-mode-setup can be enabled or disabled by passing enabled = true or false
class fips_mode_setup (Boolean $enabled = false) {
    if $enabled {
        exec { 'fips-mode-setup --enable':
            path   => '/usr/bin',
            unless => 'fips-mode-setup --is-enabled'
        }
    } else {
        exec { 'fips-mode-setup --disable':
            path   => '/usr/bin',
            onlyif => 'fips-mode-setup --is-enabled'
        }
    }
}
